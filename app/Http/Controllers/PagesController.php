<?php

namespace App\Http\Controllers;
use App\Category;
use App\Product;
use Illuminate\Http\Request;

class PagesController extends Controller
{

    public function home()
    {
        $categories = $this->getProductsCategory();
        return view('home', compact('categories'));
    }
    public function reset()
    {
        $categories = $this->getProductsCategory();
        return view('reset', compact('categories'));
    }
    public function admin()
    {
        return view('admin');
    }
}
