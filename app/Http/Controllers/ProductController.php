<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $categories = $this->getProductsCategory();
        $products = $this->getRandomProducts();
        $category ="";
        return view('products.index', compact('products', 'category', 'categories'));
    }
    public function show(Category $category)
    {
         $categories = $this->getProductsCategory();
         $products=$category->products;
         return view('products.index', compact('products', 'category', 'categories'));
    }
}
