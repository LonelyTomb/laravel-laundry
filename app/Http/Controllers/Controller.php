<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
     public function getRandomProducts()
    {
         $products = Product::limit(10)
                            ->inRandomOrder()
                            ->get();
                            return $products;
    }
    public function getProductsCategory()
    {
        $categories  = Category::all();
        return  $categories;
    }
    public function getCategoryId($category_name)
    {
        $categories  = Category::where('category_name', $category_name)->select('id')->get();
        return  $categories;
    }

}
