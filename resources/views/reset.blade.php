@extends('master')
@section('title')
<title>Reset Password</title>
@section('style')
<style>
 .reset_body{
      min-height: 250px;
    }
    .reset_body button[type="submit"]{
      margin-bottom: 2%;
    }
    </style>
@endsection
@endsection
@section('content')
<form class="form form-horizontal container" action="includes/reset_pwd.php" method="post">
          <div class="container-fluid reset_body">
          <div class="alertBlock"></div>
          <div class="form-group">
            <label for="username" class="col-xs-3 control-label">Username: </label>
            <div class="col-xs-9">
              <input type="text" id="username" name="username" class="form-control">

            </div>
          </div>
          <div class="form-group">
            <label for="email" class="col-xs-3 control-label">Email: </label>
            <div class="col-xs-9">
              <input type="email" id="email" name="email" class="form-control">

            </div>
          </div>
          <div class="form-group has-feedback">
            <label for="password" class="col-xs-3 control-label">New Password: </label>
            <div class="col-xs-9">
              <input type="password" id="rst_password" name="reset_pwd" class="form-control">
              <span class="form-control-feedback result_icn" aria-hidden="true"></span>
              <label class="result_txt"></label>
            </div>
          </div>
          <div class="form-group has-feedback">
            <label for="confrm_password" class="col-xs-3 control-label">Confirm Password: </label>
            <div class="col-xs-9">
              <input type="password" id="confrm_password" name="cnfrm_pwd" class="form-control">
              <span class="form-control-feedback result_icn" aria-hidden="true"></span>
              <label class="result_txt"></label>
            </div>
          </div>
          <div class="before_msg pull-left"></div>
          <button type="submit" class="btn btn-primary pull-right" id="reset" name="Submit">Submit</button>
        </form>
      </div>
@endsection