@extends('home')

@section('products')
<div class="well">
<h2 class="text-center clothing_header">
    @if($category == "")
    Top Laundry Orders
    @else
    {{$category->category_name}} Products
    @endif
</h2>

</div>
</div>
<div class="container">
        <table class='table table-hover table-responsive table-bordered'>
        <thead>
        <tr>
        <th class='text-center'>No</th>
        <th class='text-center'>Item</th>
        <th class='text-center'>Pricing</th>
        <th class='text-center'>Quantity</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
        <tr class='order-row'>
        <td class='item-id'>
              <p>{{$loop->iteration}}
              </p>
          </td>
         <td class='item-name'>
              <p>{{$product->name}}
              @if($category != "")
              <small class='text-info'>{{$category->category_name}}
              @endif
              </small>
              </p>
          </td>
          <td>
              <p class='pricing'>
                  &#8358;{{$product->price}}
              </p>
          </td>
          <td class='qty col-xs-3'>
              <p class='input-group'>
                  <span class='input-group-btn add' onclick='minus($(this))'><a class='btn btn-danger'><i class='glyphicon glyphicon-minus'></i></a></span>
                  <input type='text' name='quantity' class='quantity form-control' value='0'>
                  <input type='hidden' name='cloth_id' class='cloth_id' value="{{$product->id}}">
                  <span class='input-group-btn'><a class='btn btn-success' id='add' type='button' onclick="add($(this))"><i class='glyphicon glyphicon-plus'></i></a>
                      <button type='button' class='btn btn-primary' onclick='add_to_cart($(this).parent())' name='button'>Add</button></span>
              </p>
          </td>
          </tr>
@endforeach
        </tbody>
</table>
<div class="add_to_cart">
            <button type="button" class="pull-right btn btn-success" onclick="view_cart()" data-target="#laundry_cart" data-toggle="modal" name="button">View Cart</button>
        </div>
</div>
<script>
function add($item) {
    'use strict';
    var val = parseInt($item.parentsUntil('.qty').find('.quantity').val());
    val += 1;
    $item.parentsUntil('.qty').find('.quantity').val(val);
}

function minus($item) {
    'use strict';
    var val = parseInt($item.parentsUntil('.qty').find('.quantity').val());
    if (val > 0) {
        val -= 1;
        $item.parentsUntil('.qty').find('.quantity').val(val);
    }
}
</script>
@endsection
