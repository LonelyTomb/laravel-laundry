@extends('master')

@section('title')
<title>Washman</title>
@endsection

@section('Banner')
 <div class="container-fluid">
        <div class="mainalertBlock"></div>
        <div id="carousel-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carousel-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-generic" data-slide-to="1"></li>
                <li data-target="#carousel-generic" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <img src="{{ asset('images/image2.jpg')}}" alt="" class="carousel-image img-responsive ">
                    <div class="carousel-caption">

                    </div>
                </div>
                <div class="item">
                    <img src="{{ asset('images/image1.jpg')}}"  alt="" class="carousel-image img-responsive ">
                    <div class="carousel-caption">

                    </div>
                </div>
                <div class="item">
                    <img src="{{ asset('images/image3.jpg')}}"  class="carousel-image img-responsive ">
                    <div class="carousel-caption">

                    </div>
                </div>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-generic" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-generic" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container orders">
@yield('products')
</div>
@endsection