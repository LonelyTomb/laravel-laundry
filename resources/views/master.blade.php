<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1.0"> @yield('title')
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
  <link rel='prefetch' href='{{ elixir('css/app.css') }}'/>
  <link rel='stylesheet' href='{{ elixir('css/app.css') }}'/>
  <link rel='prefetch' href='{{ elixir('css/style.css') }}'/>
  <link rel='stylesheet' href='{{ elixir('css/style.css') }}'/>
  @yield('style')
  
</head>
<body>
@section('header')
  <div class="container-fluid">
    <header>
      <div class="page-header clearfix">
        <div class="col-md-3 col-xs-12 pull-left">
          <h1 class="text-center">Washman</h1>
        </div>
        <div class="col-md-4 col-md-offset-1 col-xs-offset-1 log_on">
          <ul class="nav nav-pills text-center">
          <li><a class='btn btn-info' data-toggle='modal' data-target='#logIn'>Log in</a></li>
          <li><a class='btn btn-primary' data-toggle='modal' data-target='#signUp'>Sign Up</a></li>
          </ul>
        </div>
        <div class="col-md-2 col-xs-12 pull-right cart dropdown">
          <a data-target="#laundry_cart" class="btn" data-toggle="modal" onclick="view_cart()"><h4 class="text-center">Cart: <span class="num_of_items">
        </span></h4></a>
        </div>

      </div>
      <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-content">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a href="{{ action("ProductController@index") }}" class="navbar-brand">Home</a>
          </div>

          <div class="collapse navbar-collapse" id="navbar-content">
            <ul class="nav navbar-nav">
            @foreach($categories as $category)
              <li><a href={{ url('products/'.$category->id)}}>{{$category->category_name}}</a></li>
            @endforeach
              <li><a href="#">Contact Us</a></li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
  </div>
  @show
  @yield('Banner')
  @yield('content')

@section('footer')
<footer class="container-fluid">
      <div class="col-sm-6 col-xs-12 pull-left">
          <h4 class="page-header">Laundry...</h4>
          <ul class="nav nav-pills">
              <li><a href="#" class="text-muted">About Us</a></li>
              <li><a href="#" class="text-muted">Delivery</a></li>
              <li><a href="#" class="text-muted">Terms &amp; Conditions</a></li>
          </ul>
      </div>
      <div class="col-sm-6 col-xs-12 pull-right">
          <h4 class="page-header">Account...</h4>
          <ul class="nav nav-pills">
              <li><a href="profile.php" class="text-muted">My Account</a></li>
              <li><a href="#" class="text-muted">Order History</a></li>
          </ul>
      </div>
  </footer>
@show
@section('signUp')
<div class="modal fade signUp" role="dialog" id="signUp">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" data-dismiss="modal" class="close">&times;</button>
                  <h3 class="modal-title text-center">Sign Up</h3>
              </div>
              <div class="modal-body">
                  <form class="form form-horizontal container-fluid" action="{{action("UsersController@register")}}" method="post">
                  {{ csrf_field()}}
                    <div class="alertBlock"></div>
                    @include('errors.formerrors')
                      <div class="form-group has-feedback row">
                          <label for="name_up" class="col-xs-3 control-label">Name: </label>
                          <div class="col-xs-9">
                              <input id="name_up" class="name_up form-control" name="name" type="text" placeholder="Enter one or more names"></input>
                              <span class="form-control-feedback result_icn" aria-hidden="true"></span>
                              <label class="result_txt"></label>
                          </div>
                      </div>
                      <div class="form-group has-feedback row">
                          <label for="email_up" class="col-xs-3 control-label">Email: </label>
                          <div class="col-xs-9">
                              <input id="email_up" class="email_up form-control" name="email" type="email"></input>
                              <span class="form-control-feedback result_icn" aria-hidden="true"></span>
                              <label class="result_txt"></label>
                          </div>
                      </div>
                      <div class="form-group has-feedback row">
                          <label for="tel_up" class="col-xs-3 control-label">Tel: </label>
                          <div class="col-xs-9">
                              <input id="tel_up" class="tel_up form-control" name="phone" type="text"></input>
                              <span class="form-control-feedback result_icn" aria-hidden="true"></span>
                              <label class="result_txt"></label>
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="address_up" class="col-xs-3 control-label">Address: </label>
                          <div class="col-xs-9">
                              <textarea id="address_up" class="address_up form-control" name="address"></textarea>
                              <label class="result_txt"></label>
                          </div>
                      </div>
                      <div class="form-group has-feedback row">
                          <label for="pwd_up" class="col-xs-3 control-label">Password: </label>
                          <div class="col-xs-9">
                              <input id="pwd_up" class="pwd_up form-control" name="password" type="password"></input>
                              <span class="form-control-feedback result_icn" aria-hidden="true"></span>
                              <label class="result_txt"></label>
                          </div>
                      </div>
                      <div class="before_msg pull-left"></div>
              </div>
              <div class="modal-footer">
                  <div class="form-group">
                      <button type="submit" class="btn btn-primary" name="sign_Up" id="sign_up">Sign Up</button>
                      <button type="button" class="btn btn-warning" name="button" data-dismiss="modal">Close</button>
                  </div>
              </div>
              </form>
          </div>
      </div>
  </div>

@show
@section('signIn')
<div class="modal fade signIn" role="dialog" id="logIn">
      <div class="modal-dialog">
          <div class="modal-content ">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h3 class="modal-title text-center">Log In</h3>
              </div>

              <div class="modal-body">
                  <form class="form form-horizontal container-fluid" action="includes/sign_in.php" method="post">
                      {{ csrf_field()}}
                    <div class="alertBlock"></div>
                      <div class="form-group">
                          <label for="email" class="col-xs-3">Email: </label>
                          <div class="col-xs-9">
                              <input id="email" class="form-control" name="email" type="email"></input>
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="pwd" class="col-xs-3">Password: </label>
                          <div class="col-xs-9">
                              <input id="pwd" class="form-control" name="password" type="password"></input>
                          </div>
                      </div>
                      <div class="before_msg pull-left"></div>
              </div>
              <div class="modal-footer">
                  <div class="form-group">
                    <a href="{{ action("PagesController@reset") }}" class="btn btn-info pull-left" name="reset">Forgot Password?</a>
                      <button type="submit" class="btn btn-primary"id="log_in" name="log_in">Log In</button>
                      <button type="button" class="btn btn-warning" name="button" data-dismiss="modal">Close</button>
                  </div>
              </div>
              </form>

          </div>
      </div>
  </div>
@show

@section('laundryCart')
<div class="modal fade signUp" role="dialog" id="laundry_cart">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" data-dismiss="modal" class="close">&times;</button>
                  <h3 class="modal-title text-center">Laundry Cart</h3>
              </div>
              <div class="modal-body cart_body">
              </div>
              <div class="modal-footer">
                  <button type="button" name="button" class="chk_out btn btn-success pull-left">Checkout</button>
                  <button type="button" name="button" onclick="clear_cart()" class="clear_cart btn btn-danger">Empty Cart</button>
                  <button type="button" class="btn btn-warning" name="button" data-dismiss="modal">Close</button>
                  <p class="text-center before_msg" style="border-bottom: 1px solid #ddd;padding: 1px 0 5px;">
                    Processing
                  </p>
              </div>
          </div>
      </div>
  </div>

@show

<script>
/* ******************************/
/*   Cart Functions   */
/* ******************************/
function add_to_cart ($item) {
  'use strict';
  var val = parseInt($item.parent().find('.quantity').val());
  var name = $item.parent().find('.name').val();
  var id = $item.parent().find('.cloth_id').val();
  if (val > 0) {
    $.ajax({
      type: 'POST',
      url: 'includes/process_cart.php',
      cache:false,
      data: {
        cloth_id: id,
        quantity: val,
        send:''
      },
      dataType: 'json',
      success: function (data) {
        $('.cart_body').html(data.txt);
        $('.num_of_items').html(data.count);
      }
    });
  }
  $item.parent().find('.quantity').val('0');
}

// Removes item from cart
function remove_from_cart ($item) {
  'use strict';
  var id = parseInt($item.parent().find('input').val());
  $.ajax({
    type: 'POST',
    url: 'includes/process_cart.php',
    cache:false,
    data: {
      id:id
    },
    dataType: 'json',
    success: function (data) {
      $('.num_of_items').html(data.count);
      $('.cart_body').html(data.txt);
      if ($('.cart_body tbody').html() === '') {
        $('#laundry_cart').modal('hide');
      }
    }
  });
}
// display laundry cart modal
function view_cart () {
  'use strict';
  $.ajax({
    type: 'POST',
    url: 'includes/process_cart.php',
    cache:false,
    data:{
      view:''
    },
    dataType: 'json',
    success: function (data) {
      $('.num_of_items').html(data.count);
      if (!(data.count > 0)) {
        $('.clear_cart, .chk_out').hide();
      } else {
        $('.clear_cart, .chk_out').show();
      }
      $('.cart_body').html(data.txt);
    }
  });
}
// Empties the cart
function clear_cart () {
  'use strict';
  $.ajax({
    type: 'POST',
    url: 'includes/process_cart.php',
    cache:false,
    data: {
      clear:''
    },
    dataType: 'json',
    success: function (data) {
      $('.num_of_items').html(data.count);
      $('.cart_body').html(data.txt);
      $('#laundry_cart').modal('hide');
    }
  });
}

/* ***************/
/*        End   */
/* ***************/
</script>
  <script src='{{ elixir('js/app.js') }}'></script>

</body>

</html>