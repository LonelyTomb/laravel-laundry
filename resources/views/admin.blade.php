<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
   @yield('title')
    <link rel="stylesheet" href="{{ elixir('css/app.css')}}">
    <link rel="stylesheet" href="{{ elixir('css/admin.css')}}">
  </head>
  <body>
</div>
  </div>
    <script src="script.js"></script>
  </body>
</html>
