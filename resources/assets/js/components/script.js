// toggles Alert Window on/off
function displayAlert(parent, data, color = 'alert-info', toggle = 'show') {
    var alert = '<div class="alert ' + color + ' alert-dismissible text-center" role="alert"><button type="button" class="close close_alert" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><div class="alert_msg">' + data + '</div></div></div>';

    if ($(parent).children().is('div.alert') && toggle === 'show') {
        $('div.alert_msg').html(data);
    } else if (!($(parent).children().is('div.alert')) && toggle === 'show') {
        $(parent).append(alert);
    } else if (toggle === 'hide') {
        $(alert).detach();
    }
}
// Function to toggle Validation icon
function validateIcn(icon, parent) {
    'use strict';
    if (parent.find('.result_icn').hasClass('glyphicon-remove')) {
        if (icon === 'glyphicon glyphicon-ok text-success') {
            parent.find('.result_icn').removeClass('glyphicon-remove text-danger').addClass('glyphicon-ok text-success');
        }
    } else if (parent.find('.result_icn').hasClass('glyphicon-ok')) {
        if (icon === 'glyphicon glyphicon-remove text-danger') {
            parent.find('.result_icn').removeClass('glyphicon-ok text-warning').addClass('glyphicon-remove text-danger');
        }
    } else {
        parent.find('.result_icn').addClass(icon);
    }
}

/**
 *
 *
 * @param {any} className
 * @param {any} postName
 */
function ajaxValidate(className, name) {
    'use strict';
    var _token = $('.signUp input[name="_token"]').val();
    var value = $(className).val();
    var p = $(className).parent();
    var postName = name;
    $.ajax({
        type: 'post',
        url: '/validateName',
        cache: false,
        data: {
            'name': value,
            _token: _token
        },
        dataType: 'json'
    }).done(function(data) {
        // validateIcn(data.icon, p);
        // p.find('.result_txt').show();
        // p.find('.result_txt').html(data.txt);
        console.log(data.responseText);
    }).fail(function(data) {
        // validateIcn(data.icon, p);
        // p.find('.result_txt').show();
        // p.find('.result_txt').html(data.txt);
        console.log(data.responseText[0]);
    });
}

(function() {
    'use strict';
    var val = $('result_txt').parent().find('input').val();
    if ($.trim(val) === '' && $('.result_txt').html() === '') {
        $('.result_txt').hide();
    }
    if ($(window).width() <= 480) {
        $('.delete_item').html('Del');
    }
    $(window).resize(function() {
        if ($(window).width() <= 480) {
            $('.delete_item').html('Del');
        }
    });

    if ($('form .alert_msg strong').html() === '') {
        $('form .alert').hide();
    }
    $('.name_up').blur(function() {
        ajaxValidate('.name_up', 'name');
    });
})();