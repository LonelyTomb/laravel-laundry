<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "ProductController@index");
Route::get('/reset', "PagesController@reset");
Route::get('/staff', "PagesController@admin");
//Route to Home Page
Route::get('/products', "ProductController@index");
//Route to Particular product Page
Route::get('/products/{category}', "ProductController@show");


Route::post('/validateName', "UsersController@validateName");
Route::post('/register', "UsersController@register");
Route::post('/login', "UsersController@login");
